<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Sentiment Analysis</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body>

<div class="container">
  <div class="header">
      <center><h1> Mapping Sentiments in Tweets onto the World Map </h1></center>
    </div>
    <div id="navigation">
      <ul>
        <li> <a href = "index.html"> Introduction </a></li>
        <li> <a href = "guidelines.html"> Guidelines </a></li>
        <li> <a href = "annotations.php">Annotations </a></li>
        <li> <a href = "mapping/map_display.html"> Visualise Map </a></li>
        <li> <a href = "ProgressReportPresentation.pdf"> Presentation </a></li>
      </ul>
    </div>
  
  	<div class="content">
  		<h2>Annotations</h2>

    	<form method="post" action="./update_db.php">
		<p> Please select one of the given labels for this tweet, following the <a href="guidelines.html">guidelines</a>: <br><br>
		<?php include("fetch_tweet.php"); ?>
		<!-- Need these two for update_db.php -->
		<input id = "tweetId" name = "tweetId" type = "hidden">
		<input id = "tweetText" type = "hidden">
		
		<!-- Print topic -->
		<div class="tweet">
		<script type = "text/javascript">
			var tweetId = document.getElementById("rowID").value;
			if (tweetId != "") {
				var tweetTopic = document.getElementById("rowTopic").value;
				var tweetText = document.getElementById("rowText").value;
				document.getElementById("tweetId").value = tweetId;
				document.getElementById("tweetText").value = tweetText;
				document.write("<b>Id: </b>" + tweetId + "<hr>");
				document.write("<b>Topic: </b>" + tweetTopic + "<hr>");
				document.write("<b>Tweet: </b>" + tweetText);	
			} else {
				document.write("There are currently no available tweets. Please try later.");
			}	
		</script>
		</div><br>

		<hr><p>
		<b>Label:</b> 
		<br>
		<input type = "radio" name = "label" id = "positive" value = "positive">
		<label for="positive">Positive</label><br>
		<input type = "radio" name = "label" id = "neutral" value = "neutral">
		<label for="neutral">Neutral</label><br>
		<input type = "radio" name = "label" id = "negative" value = "negative">
		<label for="negative">Negative</label><br>
		<input type = "radio" name = "label" id = "undecidable" value = "undecidable">
		<label for="undecidable">Undecidable</label><br>

		<br>
		<p><input type="submit" value="Next Tweet">
		</form>
		
		<hr><p>
		<b>Additional Information:</b>
		<br>
		<label for="irony">Mark this tweet as ironic</label>
		<input name = "irony" id = "irony" type = "checkbox"><br>
		If you find this tweet particularly hard to annotate, please leave a comment:
		<input name = "comment" type = "textarea" maxlength = "200" rows = "2">
		<hr>

		

		<form method="post" action="thanks.php">
			<input type="hidden" name="tid" id="tid">
			<script type = "text/javascript">
				var id =  document.getElementById("tweetId").value;
				document.getElementById("tid").value = id;
			</script>
			<p><input type="submit" value="Stop annotating">
		</form>
    </div>
  
  	<div class="footer">
  		<p><a href="mailto:irt24@cam.ac.uk"> Contact </a>
  	</div>
</div>
</body>
</html>
	