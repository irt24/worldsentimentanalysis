<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Sentiment Analysis</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body>

<div class="container">

  <div class="header">
      <center><h1> Mapping Sentiments in Tweets onto the World Map </h1></center>
    </div>

    <div id="navigation">
      <ul>
        <li> <a href = "index.html"> Introduction </a></li>
        <li> <a href = "guidelines.html"> Guidelines </a></li>
        <li> <a href = "annotations.php">Annotations </a></li>
        <li> <a href = "mapping/map_display.html"> Visualise Map </a></li>
        <li> <a href = "ProgressReportPresentation.pdf"> Presentation </a></li>
      </ul>
    </div>
  
  	<div class="content">
  		<form action="annotations.php" method="POST">
  			<center> Thanks for your help! </center>
  			<input type="submit" value="Go back to annotations">
  		</form>
    </div>
  
  	<div class="footer">
  		<a href="mailto:irt24@cam.ac.uk"> Contact </a>
  	</div>
</div>

<?php
// Connect to the database
  $con = mysql_connect("localhost","irt24","raluplus");
  if (!$con) {
    die ("Could not connect: " . mysql_error());
  }
  mysql_select_db("irt24/sentiment_analysis");

  // Set Sent2client back to 0
  $update = mysql_query("UPDATE Tweets SET Sent2client = 0 WHERE ID='".$_POST['tid']."'")
  or die ("Could not update Sent2client field".$POST['id']);
?>
</body>
</html>
	