Afghanistan
Kyrgyzstan
Tajikistan

Albania
Andorra
Bosnia and Herzegovina
Croatia
Italy
Macedonia
Malta
Serbia and Montenegro
Tunisia

Algeria
Libya

Angola
Congo
Congo Democratic Republic
Gabon
Kiribati

Anguilla
Antigua and Barbuda
Barbados
Dominica
Grenada
Guadeloupe
Martinique
Montserrat
Netherlands Antilles
Puerto Rico
Saint Kitts and Nevis
Saint Lucia
Saint Vincent and the Grenadines
Virgin Islands (British)
Virgin Islands (U.S.)

Argentina

Armenia
Azerbaijan
Turkmenistan
Uzbekistan

Aruba
Belize
Cayman Islands
Dominican Republic
El Salvador
Haiti
Honduras
Jamaica
Nicaragua
Turks and Caicos Islands

Australia

Austria
Belgium
Czech Republic
Denmark
France
Germany
Hungary
Liechtenstein
Luxembourg
Netherlands
Poland
Slovakia
Slovenia
Switzerland

Bahamas

Bahrain
Kuwait
Qatar
Saudi Arabia
United Arab Emirates

Bangladesh
Bhutan

Belarus
Lithuania
Moldova
Romania
Ukraine

Bolivia
Brazil

Botswana
Comoros
Malawi
Mozambique
Zambia
Zimbabwe

Bouvet Island
Ethiopia
Macau
Uganda

Brunei Darussalam
Palau

Bulgaria
Cyprus
Georgia
Greece
Lebanon
Turkey

Burkina Faso
Gambia
Guinea-Bissau
Mali
Mauritania
Senegal

Burundi
Kenya
Rwanda
Tanzania

Cambodia
Lao People''s Democratic Republic
Thailand
Viet Nam

Chad
Niger
United States Minor Outlying Islands

Chile

China

Christmas Island

Colombia
Costa Rica
Panama

Djibouti
Eritrea

East Timor
Indonesia

Ecuador
Peru

Egypt
Iraq
Israel
Jordan
Palestinian Territory

Estonia
Finland
Latvia

Faroe Islands
Iceland

French Guiana
Guyana
Suriname
Trinidad and Tobago
Venezuela

French Southern Territories

Ghana
Guinea
Liberia
Sierra Leone

Gibraltar
Portugal
Spain

Guatemala

Hong Kong
Philippines

India

Ireland
Monaco

Kazakhstan

Lesotho
Swaziland

Madagascar
Mauritius
Mayotte
Reunion

Malaysia
Singapore

Maldives
Sri Lanka

Mexico

Mongolia

Morocco
Western Sahara

Namibia

Nauru

Nepal
Pakistan

New Caledonia
Vanuatu

Norway
Sweden

Oman
Yemen

Papua New Guinea

Paraguay
San Marino
Uruguay

Seychelles

Somalia

South Africa

South Korea

St. Pierre and Miquelon

Taiwan

United Kingdom

United States

Vatican

