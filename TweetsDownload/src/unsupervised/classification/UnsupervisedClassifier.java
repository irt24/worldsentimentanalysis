package unsupervised.classification;

import auxiliaries.FileIO;
import auxiliaries.Pair;
import auxiliaries.Polarity;
import java.io.BufferedWriter;

/**
 * Evaluator regarding the performance of the unsupervised system
 */
public class UnsupervisedClassifier {
    
    private static final String dictionariesPath = "text_files\\dictionaries\\";
    private static final String statisticsFile = "text_files\\statistics";
    private static BufferedWriter out;
    
    private static Polarity getPolarityScheme(int index) {
        // For dictionary3, polarity object can only be created when the scores
        // of the whole sentence are known.
        if (index == 3) return null;
        if (index == 4) {
            Pair positiveBounds = new Pair(7.0, 9.0);
            Pair neutralBounds = new Pair(4.0, 7.0);
            Pair negativeBounds = new Pair(1.0, 4.0);
            Polarity polarity = new Polarity(positiveBounds, neutralBounds, negativeBounds);
            return polarity;
        }
        // for dictionaries 1 and 2:
        return new Polarity(0);
    }
    
    private static String evaluate(String topic) throws Exception{
        String whereClause = " WHERE TOPIC = '" + topic + "'";
        Evaluator eval = new Evaluator(whereClause);
        int nMatches = eval.getNumberOfMatches();
        int nTests = eval.getNumberOfTests();
        float percent = eval.getPercentage();
        float coverage = eval.getCoverage();
        float overall = eval.getOverallPercentage();
        String toPrint = topic + ": " +
                    percent + "% Accuracy" + " (" + nMatches +
                    " matches out of " + nTests + ")" + 
                    "\t Coverage: " + coverage + "%" +
                    "\t Overall Accuracy: " + overall + "%";
        return toPrint;
    }
    
    public static void classifyWithDict(int dictionaryIndex) throws Exception{
        // Prepare datastructures needed for label prediction
        String dictionaryName = "dictionary" + dictionaryIndex;
        System.out.println("Predict labels using " + dictionaryName);
        Polarity polarity = getPolarityScheme(dictionaryIndex);
        
        // Perform label prediction
        LabelPredictor labelPrediction = 
                new LabelPredictor(dictionariesPath + dictionaryName, polarity);
        labelPrediction.predictAllLabels();
        
        // Evaluate label prediction
        String[] topics = {"BieberManual", "ObamaManual", "SandyManual", 
            "emoticon", "timeline", "newspaper"};
        String toPrint;
        for (String topic: topics) 
        {
            toPrint = evaluate(topic);
            out.write(dictionaryName + ":\t\t" + toPrint +
                    System.getProperty("line.separator"));
        }
    }
    
    public static void classifyWithAllDicts() throws Exception{
        int[] dictionaries = {1, 2, 3, 4, 12, 21};
        for (int d : dictionaries)
            classifyWithDict(d);
    }

    public static void main(String[] args) throws Exception{
        out = new FileIO().getBufferedWriter(statisticsFile); 
        
        //test(1);
        //test(2);
        //test(3);
        //test(4);
        classifyWithAllDicts();
        
        out.close();
     }
}
