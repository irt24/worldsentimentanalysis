package unsupervised.classification;

import auxiliaries.DBUtils;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class evaluates the performance of the unsupervised system by counting 
 * the number of correctly labeled Tweets.
 */
public class Evaluator {
    
    private int nTweets;
    private int nMatches;
    private int nCoveredTweets;
    private float accuracy;
    private float coverage;
    private float overallAccuracy;
    
    public Evaluator(String whereClause) throws SQLException{
        String selectionString = 
                "SELECT LABEL, POLARITY, DICTIONARY_WORDS FROM SUPERVISED" + whereClause;
        ResultSet resultSet = new DBUtils().select(selectionString);
        nCoveredTweets = 0;      // total number of tweets
        nMatches = 0;    // number of tweets that were correctly labeled
        nTweets = 0;
        while (resultSet.next()) {
            nTweets++;
            if (resultSet.getInt("DICTIONARY_WORDS") != 0)
                nCoveredTweets++;
            String label = resultSet.getString("LABEL");
            String predictedLabel = resultSet.getString("POLARITY");
            if (label.equals(predictedLabel)) {
                nMatches++;
            }
        }
        if (nCoveredTweets != 0) 
            accuracy = 100 * nMatches/nCoveredTweets;
        else accuracy = 0;
        if (nTweets != 0) {
            coverage = 100*nCoveredTweets/nTweets;
            overallAccuracy = 100*nMatches/nTweets;
        } else {
            coverage = 0;
            overallAccuracy = 0;
        }
        overallAccuracy = 100*nMatches/nTweets;
    }
    
    public int getNumberOfMatches() {
        return nMatches;
    }
    
    public int getNumberOfTests() {
        return nCoveredTweets;
    }
    
    public float getPercentage() {
        return accuracy;
    }
    
    public float getCoverage() {
        return coverage;
    }
    
    public float getOverallPercentage() {
        return overallAccuracy;
    }
}
