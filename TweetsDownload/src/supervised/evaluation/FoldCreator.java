package supervised.evaluation;

import auxiliaries.DBUtils;
import auxiliaries.FileIO;
import auxiliaries.Pair;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeSet;
import supervised.preprocessing.CentralisedDictionary;
import supervised.preprocessing.SFormattedTweet;
import preprocessing.StopWords;

/**
 * This class will create the LIBSVM-format files for the folds
 * So far, the features are binary: 1 if the word occurs in the Tweet and 0 otherwise.
 * 0-valued features need not be written to the data file.
 */
public class CreateFolds {
    
    private static final int nFolds = 10;
    private static final String path = "text_files\\supervised\\";
    private static TreeSet<String> stopWords;
    private static DBUtils db;
    private static FileIO io;
    
    public static String getLabelValue(String label) {
        if (label.equals("positive")) return "1";
        if (label.equals("neutral")) return "0";
        return "-1";
    }

    public static void createFold(int n, HashMap<String, Integer> rankMap) throws Exception{
        String rankFileName = path + "rankFolds\\fold" + n;
        String freqFileName = path + "freqFolds\\fold" + n;
        BufferedWriter out1 = io.getBufferedWriter(rankFileName);
        BufferedWriter out2 = io.getBufferedWriter(freqFileName);

        // Fetch Tweets from the database
        String selectString = "SELECT TEXT, LABEL FROM SUPERVISED WHERE FOLD_INDEX=" + n;
        ResultSet resultSet = db.select(selectString);
        while (resultSet.next()) {
            // Print the label value
            String toWrite = getLabelValue(resultSet.getString("LABEL")) + "\t";
            out1.write(toWrite);
            out2.write(toWrite);
            // Print the features: 
            // For rank: <rank> <1>, where 1 indicates the presence of the word
            // For freq: <rank> <freq>
            SFormattedTweet ft = new SFormattedTweet(resultSet.getString("TEXT"), stopWords);
            TreeSet<String> words = ft.getUniqueWords();
            HashMap<String, Integer> freqMap = ft.getWordsAndFreq();
            ArrayList<Pair<Integer, Integer>> vals = new ArrayList<>();
            for (String word : words) {
                double rank = rankMap.get(word);
                int freq = freqMap.get(word);
                vals.add(new Pair(rank, freq));
            }
            Collections.sort(vals);
            for (Pair pair : vals) {
                out1.write(pair.getLeft() + ":1\t");
                out2.write(pair.getLeft() + ":" + pair.getRight() + "\t");
            }
            out1.write(System.getProperty("line.separator"));
            out2.write(System.getProperty("line.separator"));
        }
        out1.close();
        out2.close();
    }
    
    public static void mergeFolds(String folder) throws Exception{
        FileIO io = new FileIO();
        BufferedReader[] ins = new BufferedReader[nFolds];
        BufferedWriter[] outs = new BufferedWriter[nFolds];
        for (int i = 0; i < nFolds; i++) {
            ins[i] = io.getBufferedReader(path + folder + "\\fold" + (i+1));
            outs[i] = io.getBufferedWriter(path + folder + "\\foldWithout" + (i+1));
        }
        for (int i = 0; i < nFolds; i++) {
            BufferedReader in = ins[i];
            String line;
            while((line = in.readLine()) != null) {
                for (int j = 0; j < nFolds; j++)
                    if (i != j) 
                        outs[j].write(line + System.getProperty("line.separator"));
            }
        }
        for (int i = 0; i < nFolds; i++)
            outs[i].close();
    }
    
    public static void main(String[] args) throws Exception{
        db = new DBUtils();
        io = new FileIO();
        stopWords = new StopWords().getWords();
        HashMap<String, Integer> rankMap = 
                new CentralisedDictionary("SUPERVISED").getRankHashMap();
        for (int i = 0; i < nFolds; i++) {
            createFold(i+1, rankMap);
        }
        mergeFolds("rankFolds");
        mergeFolds("freqFolds");
    }
}