package supervised.evaluation;

import auxiliaries.FileIO;
import de.bwaldvogel.liblinear.Feature;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Model;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;
import java.io.BufferedReader;
import java.io.File;

/**
 * Evaluate the supervised system using the 10-fold technique
 */
public class Evaluation {
    
    private static final int bias = 1;
    private static final int nFolds = 10;
    private static final String path = "text_files\\supervised\\rankFolds\\";
    
    private static Model getModel(int index, Parameter parameter) throws Exception{
        File file = new File(path + "foldWithout" + index);
        Problem problem = Problem.readFromFile(file, bias);
        return Linear.train(problem, parameter);
    }
    
    private static int prediction(int index, Model model) throws Exception{
        String fileName = path + "fold" + index;
        BufferedReader in = new FileIO().getBufferedReader(fileName);
        String line;
        int count = 0;
        while ((line = in.readLine()) != null) {
            String[] items = line.split("\\t");
            // Create feature vector.
            // Ignore items[0], that is the label
            Feature[] instance = new Feature[items.length - 1];
            for (int i = 1; i < items.length; i++) {
                int featureIndex = Integer.parseInt(items[i].split(":")[0]);
                FeatureNode featureNode = new FeatureNode(featureIndex, 1);
                instance[i - 1] = featureNode;
            }
            // Make prediction
            double prediction = Linear.predict(model, instance);
            int realLabel = Integer.parseInt(items[0]);
            if (realLabel == prediction) count ++;
        }
        return count;
    }
    
    public static void main(String[] args) throws Exception{
        
        SolverType solver = SolverType.L2R_L2LOSS_SVC_DUAL;
        double C = 0.125;   // cost of constraints violation
        double eps = 0.3;   // stopping criteria
        Parameter parameter = new Parameter(solver, C, eps);
        
        int[] predictions = new int[nFolds];
        for (int i = 1; i <= nFolds; i++) {
            Model model = getModel(i, parameter);
            predictions[i - 1] = prediction(i, model);
        }
        
        int sum = 0;
        for (int i = 0; i < nFolds; i++) {
            System.out.println("Score for set " + (i+1) + ": " + predictions[i]);
            sum += predictions[i];
        }
        System.out.println("Average score: " + sum/nFolds);
    }
}