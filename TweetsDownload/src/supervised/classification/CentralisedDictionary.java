package supervised.preprocessing;

import preprocessing.StopWords;
import auxiliaries.DBUtils;
import auxiliaries.Pair;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

/**
 * This class represents a centralised dictionary for the supervised system,
 * taking into consideration all the words that come up in the data corpus.
 */
public class CentralisedDictionary {
    
    // Bag of stop words (function words that do not carry any meaning)
    private TreeSet<String> stopWords = new StopWords().getWords();
    
    // All words in the data corpus that are not stop words
    private ArrayList<String> allWords = new ArrayList<>();
    
    // A hash map with key = word and value = frequency of word in data corpus
    private HashMap<String, Integer> freqHashMap = new HashMap<>();
    
    // A list of all (word, frequency) pairs, sorted descendingly on frequency
    private ArrayList<Pair<Integer, String>> freqList = new ArrayList<>();
    
    // A hash map with key = word and value = rank of word in the dictionary
    private HashMap<String, Integer> rankHashMap = new HashMap<>();

    private void readTweets(String table) throws Exception {
        String selectString = "SELECT ORIGINAL_TEXT FROM " + table;
        DBUtils db = new DBUtils();
        ResultSet resultSet = db.select(selectString);
        while (resultSet.next()) {
            String text = resultSet.getString("ORIGINAL_TEXT");
            SFormattedTweet ft = new SFormattedTweet(text, stopWords);
            allWords.addAll(ft.getBagOfWords());
        }  
    }
   
    private void computeFrequencies() throws Exception {
        // Create the (word, frequency) hash map
        for (String word : allWords) {
            Integer frequency = freqHashMap.put(word, 1);
            if (frequency != null) {
                frequency += 1;
                freqHashMap.remove(word);
                freqHashMap.put(word, frequency);
            }
        }
        
        // Create the list of (word, frequency) pairs
        Iterator it = freqHashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            freqList.add(new Pair((Comparable)pair.getValue(), (Comparable)pair.getKey()));
        }
        Collections.sort(freqList, Collections.reverseOrder());
        
        // Create the (word, rank) hash map
        int rank = 1;
        for (Pair pair : freqList) {
            rankHashMap.put((String)pair.getRight(), rank);
            rank++;
        }
    }
    
    public CentralisedDictionary(String table) throws Exception{
        readTweets(table);
        computeFrequencies();
    }
    
    public HashMap<String, Integer> getRankHashMap() {
        return rankHashMap;
    }
}