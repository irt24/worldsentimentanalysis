package supervised.classification;

import auxiliaries.DBUtils;
import auxiliaries.FileIO;
import auxiliaries.Pair;
import java.io.BufferedWriter;
import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeSet;
import supervised.preprocessing.CentralisedDictionary;
import supervised.preprocessing.SFormattedTweet;
import preprocessing.StopWords;

/**
 * This class represents an input file for the Support Vector Machine
 */
public class DataFile extends File{
    
    // The name of the data file
    private String fileName;
    
    // Dictionary containing all the words in the corpus
    private CentralisedDictionary dictionary;
    
    // Hash map with key = line index in data file, and value = tweet id
    private HashMap<Integer, String> line2idMap = new HashMap<>();
    
    private void insertData() throws Exception{
        FileIO io = new FileIO();
        BufferedWriter out = io.getBufferedWriter(fileName);
        
        DBUtils db = new DBUtils();
        String selectionString = "SELECT ID, FORMATTED_TEXT FROM TWITTER_DATA";
        ResultSet resultSet = db.select(selectionString);
        
        dictionary = new CentralisedDictionary("TWITTER_DATA");
        HashMap<String, Integer> wordsAndRank = dictionary.getRankHashMap();
        TreeSet<String> stopWords = new StopWords().getWords();
        
        int count = 0;
        while (resultSet.next()) {
            count++;
            
            // Get tweet data
            String id = resultSet.getString("ID");
            String text = resultSet.getString("FORMATTED_TEXT");
            
            // Compute (rank:frequency) pairs
            SFormattedTweet ft = new SFormattedTweet(text, stopWords);
            TreeSet<String> words = ft.getUniqueWords();
            HashMap<String, Integer> wordsAndFreq = ft.getWordsAndFreq();
            ArrayList<Pair<Integer, Integer>>  rankAndFreq = new ArrayList<>();
            for (String word : words) {
                int freq = wordsAndFreq.get(word);
                int rank = wordsAndRank.get(word);
                rankAndFreq.add(new Pair(rank, freq));
            }
            Collections.sort(rankAndFreq);
            
            // Prevent blank rows and store (tweetID, line) correspondence
            if ((count != 1)&&(!rankAndFreq.isEmpty())) {
                out.write(System.getProperty("line.separator"));
                line2idMap.put(count, id);
            }
            if ((count == 1)&&(!rankAndFreq.isEmpty()))
                line2idMap.put(count, id);
                    
            // Write values to file
            for (Pair pair : rankAndFreq) {
                out.write(pair.getLeft() + ":" + pair.getRight() + "\t");
            }
        }
        out.close();
    }
    
    public DataFile(String fileName) {
        super(fileName);
        this.fileName = fileName;
        try {
            insertData();
        } catch(Exception e) {
            System.out.println("Warning: the input file for the Support Vector Machine is empty");
            e.printStackTrace();
        }
    }
    
    public HashMap<Integer, String> getLineToIdMap() {
        return line2idMap;
    }
}
