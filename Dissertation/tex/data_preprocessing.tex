\section{Data Preprocessing}
\label{data_preprocessing}
Tweets are noisy, in the sense that their text suffers from deviations from the normal standards of writing. This section describes the preprocessing operations performed on text in order to make it operable, followed by a code overview. 
\subsection{Preprocessing Operations}
\label{operations}
As suggested by \cite{link3} and \cite{indiansentiwordnet} , the following have been done:
\begin{enumerate}
	\item
	Characters were converted to lower-case. Although capitalisation cannot be an indicator of sentiment polarity any more, this transformation makes data handling much easier;
	\item
	Emoticons were removed from those tweets that were collected using emoticon queries (as mentioned in \autoref{data_annotation});
	\item 
	Emoticons present in the remaining tweets were substituted with their corresponding meaning in words. For this purpose, an emoticons dictionary was used\footnotemark[1];
	\footnotetext[1]{The Internet Lingo Dictionary \\ \text{ } \quad (http://www.ag.idaho.gov/publications/internetSafety/InternetLingoDictionary.pdf).}
	\item
	All URLs were eliminated;
	\item
	All \emph{usernames} were deleted. Usernames are tokens that start with the character `@', followed by alphanumeric characters; 
	\item
	All twitter-specific tokens were eliminated. Examples include `RT' (Retweet) or `MT' {Modified tweet};
	\item
	All remaining non-alphanumeric characters were substituted by spaces. The reason why they were not completely deleted is that they might form the boundary between words. The number of exclamation and question marks was stored for the supervised classifier;
	\item
	Individual terms were tokenised, using a library for tokenisation\footnotemark[2];
	\footnotetext[2]{The Stanford Tokenizer (http://nlp.stanford.edu/software/tokenizer.shtml).}
	\item
	Letter repetition has been eliminated. In cases of three or more repetitions of one letter, a dictionary was consulted to establish the right number of consecutive occurrences of the letter. The number of elongated words (words with repeating letters) was stored for the supervised classifier;
	\item
	Each term was replaced with its \emph{lemma} (the dictionary form), using the library mentioned above\footnotemark[2];
	\item
	A number of 342 \emph{stop words} (common, short grammatical words, with no emotional content) were eliminated when preprocessing tweets for the unsupervised classifier. Evaluation shows that keeping the stop words for the supervised classifier increases accuracy.
\end{enumerate}
The following examples illustrate the data preprocessing operations on tweets taken from the data corpus:
\begin{itemize}
	\item 
	Before: \emph{RT @TelegraphSport: Andy Murray is through to \#ausopen final. Beats Roger Federer for first time in a grand slam 6-4, 6-7, 6-3, 6-7, 6-2 ...}\\
	After: \emph{andy murray ausopen final beat roger federer time grand slam}
	\item
	Before: \emph{RT @absssXD: Get me back to London, I'm missing getting the tube everywhere! :-(} \\
	After: \emph{london miss tube sad}
	\item
	Before: \emph{@hannerr\_smith @Kaii\_Smith I miss you already :( we MUST sort something for half term week! $<$3 $<$3} \\
	After: \emph{miss sad sort half term week love love}
\end{itemize}
Related work mentions the eliminations of hashtags\footnote{Hashtags are tokens that start with the character `\#', followed by alphanumeric characters that summarise the topic of the tweet, or make reference to another topic.} \cite{link3}. However, no justification for this choice is provided, so a decision has been made not to remove them. The reason is that keeping these alphanumeric components increases accuracy by providing additional information related to the polarity of the tweet. The example below illustrates this situation:
\begin{itemize}
	\item 
	Before: \emph{Try introducing these \#habits, they may make you happier! : ) \#GoodIntentions \#prioritise \#expectations \#happiness} \\
	After: \emph{try introduce habit make happy good intention prioritise expectation happy}
\end{itemize}
\subsection{Code Overview}
\label{preprocessing_code_overview}
The diagram below describes the class structure involved in the data preprocessing step.
\begin{figure}[H]
\includegraphics[width=\textwidth]{img/data_preprocessing/preprocess.pdf}
\caption{Data Preprocessing}
\label{preprocess}
\end{figure}
The \emph{ProcessedTweet} class adds new state to the basic \emph{Tweet} class, encapsulating the transformations of the text. It is then further extended, to provide appropriate text formatting for each type of classifier. A \emph{Processor} applies the data preprocessing operations on all the messages in the database. The two auxiliary classes (\emph{EmoticonDictionary} and \emph{StopWordList}) are a compact representation of data stored in files. 
\vsmallspace
As a result of this process, two columns are hence updated for each tweet in the database. Formatted texts are represented as comma-separated words. Tokens might be preceded by special symbols to convey more information about them. For example, words preceded by `!' are negated, and the ones starting with `E' are elongated.