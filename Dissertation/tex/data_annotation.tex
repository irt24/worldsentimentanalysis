\newpage
\section{Data Annotation}
\label{data_annotation}
The process of \emph{annotation} consists of associating a tweet with the corresponding sentiment orientation tag. Such data is necessary for two main reasons, namely training the supervised system (see \autoref{supervised}) and evaluation of the performance achieved by the sentiment polarity classifiers (see \autoref{evaluation}). For this purpose, two strategies were adopted:
\begin{enumerate}
	\item 
	Data with noisy labels was automatically collected.
	\item
	A representative random sample of tweets were annotated for their true sentiment by human judges.
\end{enumerate}

\subsection{Noisy Labels}
\label{noisy_labels}
The first approach uses the \emph{Twitter Search API} (described in \autoref{search_api}) to collect readily-annotated data. Two methods were used to achieve this, as described below.
\subsubsection{Emoticon queries}
This method consists of setting the track words to be `:)' and automatically labelling the tweets retrieved as being positive. Similarly, the keyword `:(' is used to fetch negatively-tagged messages. These labels are called \emph{noisy} because, other than the presence of emoticons, they might not contain any further proof of polarity. tweets such as \emph{You hacked my phone :)}, or \emph{It's mine ! :(} illustrate this shortcoming. \\[3pt]
Using this method, two subcorpora were built. The first one will be referred to as the \emph{Experimental Corpus}, because it was used for observing where the system fails so that improvements could be made. The second one is part of the \emph{Training and Evaluation Corpus}, and will be named \emph{Emoticon Subcorpus} from here on. Corpora contain 2000 tweets each, out of which 1000 are positive and 1000 are negative.
\subsubsection{User timelines}
An alternative solution for collecting data with noisy labels is to manually look for user accounts that contain posts of a predominant polarity. For instance, newspapers represent a reliable source of neutral text and charities post encouraging and optimistic messages. Negative tweets are significantly harder to find and are much noisier. However, a set of timelines has been selected for this purpose, belonging to people who dedicate their accounts to sharing their suffering. The motivation for a second method of collecting readily-annotated data is that, even though the first method is recommended in a number of research papers, a simple screening shows that the data is extremely noisy. Appendix 2 contains a list of all user accounts used for this purpose. These tweets are part of the \emph{Training and Evaluation Corpus} and will be referred to as the \emph{Timeline Subcorpus} from here on. \\[3pt]

\subsection{Manual Labels} 
\label{manual_labels}
This subsection describes the second method for annotating data, which involved a number of 10 volunteers recruited for manual annotation. Participants have objectively attempted to decide the nature of the message, without expressing any personal opinion on the selected topics, even if in disagreement with the author's point of view. The decisions were solely based on the language contained in the tweets. 
\subsubsection{Noise Elimination}
In order to eliminate subjectivity and noise, the following measures have been taken:
\begin{itemize}
	\item 
	Each message has been reviewed by three different annotators, and the most popular tag was chosen to be the final label. tweets associated with three distinct labels were eliminated from the data corpus.
	\item
	The annotators were provided with the option to mark a message as `undecidable' and were advised to use it when facing serious difficulties in understanding the text or when a decision could simply not be made. These tweets were deleted from the corpus.
	\item
	Participants had the option of marking a post as `ironic'/`metaphorical', in situations where the polarity was clear to the human annotator, but a more complex inference process was required to understand the true semantics. These tweets were also deleted from the corpus.
	\item
	Volunteers were able to leave comments, specifying any difficulties. Their notes were considered and a decision has been made on whether to keep the corresponding tweet or remove it from the corpus.  
\end{itemize}
Previous work on sentiment analysis such as \cite{annotations} invests more effort into enhancing the quality of annotations. Researchers often use a series of filters to detect poor annotators, such as time filters (ignore batches of messages that took too much time to be labelled) or sloppiness filters (insert one trivial annotation task in every 10 posts and discard those batches that label it incorrectly). However, their experiments rely on potentially unreliable workers, employed through systems like Amazon Mechanical Turk\footnotemark[1]\footnotetext[1]{https://www.mturk.com/mturk/welcome}. It is not the case here to take further measures.
\subsubsection{Graphical Interface for Manual Annotation}
In order to provide annotators with a graphical interface for the process of annotation, a website has been built (Figure \ref{annotation}). The volunteers were acquainted to the aims of the project (the `Introduction' tab) and were given guidelines on how to choose the labels (the `Guidelines' tab). Next, they were invited to proceed to annotation: given the text and topic of a tweet, they could opt for one of the labels and mark the message as described above (the `Annotations' tab).
\begin{figure}[H]
\centering
	\includegraphics[scale=0.7]{img/annotations_browser.png}
\caption{Graphical interface for manual annotation}
\label{annotation}
\end{figure}
\subsubsection{The Manually-Annotated Subcorpus}
The tweets annotated through the previously-described system form the \emph{Manually-Annotated Corpus} (which is part of the \emph{Training and Evaluation Corpus}) and cover the following topics:
\begin{itemize}
	\item 
	\emph{Justin Bieber}, the person who owns the most followed Twitter account. tweets on this topic are interesting from a sentiment analysis point of view, first because they show very strong polarities and, in the same time, make room for neutral messages (such as the ones that announce concerts). Second, the target audience consists of teenagers, who use slang, emoticons and other deviations from normal standards of writing.
	\item 
	\emph{Barack Obama}, the most followed politician on Twitter. This topic has been chosen because it is approached by a different social class and age group, which results in a distinct locution. The vocabulary is significantly more complex and there are fewer informal proofs of polarity, such as emoticons or letter repetitions. 
	\item 
	\emph{Hurricane Sandy}, the most followed event at the time of data collection (October 2012). Opinions related to an event are significantly different in phrasing than the ones related to people. Also, this topic in particular has the potential of being challenging for sentiment analysis, because the presence of negative terms such as \emph{hurricane} or \emph{disaster} can mislead the system into thinking that positive, encouraging tweets are negative (e.g. \emph{My prayers have been offered for all those affected by hurricane Sandy. Stay safe.}). 
	\item
	\emph{Microsoft Surface}, the newest series of tablets designed and marketed by Microsoft. This topic has been chosen in order to test the ability of the system to judge the success of a product on the market. Correctly labelling tweets on this topic is a hard task for sentiment analysis, because many messages objectively present a favourable or unfavourable review of the product, from the point of view of another company (e.g. \emph{Is Nokia starting to believe in the Surface phone?}).
\end{itemize}
A number of 1000 tweets were proposed for manual annotation, out of which 500 were included in the corpus. The other 500 messages were removed because of the measures taken to eliminate noise.
\subsubsection{Review of the Data Corpus}
This section introduced new taxonomy related to the data corpus, so a review of its components is necessary. Figure \ref{data_corp} illustrates how the sub-corpora are related.
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{img/data_collection/corpus.pdf}
\caption{Review of the Data Corpus}
\label{data_corp}
\end{figure}