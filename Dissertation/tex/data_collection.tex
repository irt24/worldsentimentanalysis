\section{Data Collection}
\label{data_collection}
This section describes the implementation of the data collection engine, for both approaches presented in \autoref{data_collection_preparation}, which make use of the \emph{Twitter API}.

\subsection{The \emph{Twitter 4J Library}}
\label{twitter4j}
The communication with the microblogging platform was managed through \emph{Twitter 4J}\footnote{http://twitter4j.org/en/index.html}, a Java library for the \emph{Twitter API}. The main reasons for using a library rather than implementing everything from scratch are listed below:
\begin{itemize}
	\item 
	The data collection engine must access server resources on behalf of a Twitter account owner, and this delegation is only possible in the presence of the \emph{OAuth} (\emph{Open Authorization}) protocol and an access token. The implementation of this protocol is strongly discouraged by Twitter\footnote{https://dev.twitter.com/docs/auth/using-oauth}, due to the fact that it is an error-prone process.
	\item 
	Speed up the coding process by making use of already-implemented functionality. For instance, making a query is as simple as declaring an object of type \emph{Query} and setting its member fields (see \autoref{searchAPI}).
\end{itemize}
The choice of \emph{Twitter 4J} in particular is motivated by the fact that it is written in Java and therefore provides a clean integration with the project. Also, it is compatible with the newest version of the \emph{Twitter API} (version 1.1). Furthermore, built-in support for \emph{OAuth} is provided and there are no external dependencies on other libraries.

\subsection{Data Collection Using the \emph{Twitter Search API}}
\label{searchAPI}
As mentioned in \autoref{search_api}, the \emph{Twitter Search API} was first used in the attempt to obtain a uniform, fine-grained geographical distribution of tweets. Because of its unsatisfactory performance, its usage was limited to collecting small sets of data that satisfy a well-defined set of parameters. This section describes in more detail the usage of the API and presents the implementation of the two applications.

\subsubsection{General framework} 
Both applications follow the same workflow. First, an access token is obtained, which enables the application to act on behalf of a Twitter user. This action is required only once, as the token does not expire.\footnote{https://dev.twitter.com/docs/auth/oauth/faq} Then, before each execution, the applications need to authenticate using the protocol mentioned above. Finally, queries are made and data is collected. The diagram below illustrates this process.
\begin{figure}[H]
\includegraphics[width=\textwidth]{img/data_collection/searchAPI.pdf}
\caption{Workflow for the \emph{Twitter Search API}.}
\end{figure}
The rest of this section focuses on the last two steps of the workflow, namely making queries and storing data.

\subsubsection{Collecting Data for Mapping}
The process of collecting data to be plotted on a map consists in performing a set of queries for each topic of interest. A set of queries contains one query for each geographical location considered.
\vsmallspace
First, a decision had to be made on the granularity of the geographical distribution. It was chosen to consider all countries and American states as individual points of interest. From here on, each location will be simply referred to as `state'. Hence a complete list of states had to be collected\footnote{http://countrycode.org/ and http://www.50states.com/}. Next, the \emph{Yahoo! PlaceFinder} API\footnote{http://developer.yahoo.com/boss/geo/} was involved in retrieving the geographical coordinates associated with each location. These values were stored in the database, in the \emph{Places} table (see \autoref{database}).
\vsmallspace
Second, the query mechanism associated with each topic was designed. It makes use of the \emph{Iterator} design pattern, because it must iterate over the collection of places. The choice to abstract away the representation of places is due to the fact that later decisions to change the geographical granularity might determine changes in their structure.
\begin{figure}[H]
\includegraphics[width=\textwidth]{img/data_collection/queryIterator.pdf}
\caption{The \emph{Iterator Design Pattern} used for query generation.}
\label{query}
\end{figure}
Finally, a \emph{ComplexDownloader} object iterates over all topics. For each of them, it uses the \emph{QueryIssuer} in a loop, until all queries for that topic have been made (that is, all geographical locations have been visited). Figure \ref{query2} illustrates the aggregation relation between the \emph{Downloader}, which uses multiple instances of \emph{Query}, and the composition relation between a \emph{Query} and its unique \emph{GeoLocation}.
\begin{figure}
\centering
\includegraphics[scale = 0.8]{img/data_collection/twitter_query.pdf}
\caption{Queries through the \emph{Twitter Search API}.}
\label{query2}
\end{figure}

\subsubsection{Collecting small sets of data}  
Limited amounts of data are obtained through two engines that make use of \emph{Query} objects to request messages from the \emph{Twitter} server:
\begin{enumerate}
	\item 
	\emph{SimpleDownloader} - gathers tweets on a certain topic and stores them in the database;
	\item 
	\emph{TimelineDownloader} - fetches up to 200 tweets from a specified user timelines. 
\end{enumerate}

\subsection{Data Collection Using the \emph{Twitter Streaming API}}
The \emph{Twitter Streaming API} was used as an alternative for collecting data to be plotted on the world map, after the \emph{Twitter Search API} failed to provide the desired geographical granularity. As described in \autoref{streaming_api}, the two APIs work on different principles, so the implementation of a new data collection engine was required. This section explains how this engine was coded and employed in gathering significant amounts of data.
\subsubsection{General Framework}
The functionality of this system ensures authentication, establishes a permanent connection with the \emph{Twitter} server, and then continuously receives and handles data by storing it into a database (see the diagram below).
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{img/data_collection/streamingAPI.pdf}
\caption{Workflow of the \emph{Twitter Streaming API}}
\label{streaming_workflow}
\end{figure}
\subsubsection{Implementation Details}
The first two steps are identical to the ones performed by the \emph{Twitter Search API}. As far as the connection establishment is concerned, \emph{Twitter} provides three option for endpoints\footnote{https://dev.twitter.com/docs/streaming-apis/streams/public}, enumerated below: 
\begin{enumerate}
	\item 
	\emph{POST statuses/filter} - `returns public statuses that match one or more filter predicates';
	\item 
	\emph{GET statuses/sample} - `returns a small sample of all public status';
	\item 
	\emph{GET statuses/firehose} - `returns all public statuses'.
\end{enumerate}
The collection engine presented in this section makes use of the first endpoint, because tweets need to be filtered by topic. 
\vsmallspace
Establishing and keeping a persistent HTTP connection open is possible through a \emph{TwitterStream} object, provided by the \emph{Twitter 4J} library. A \emph{StatusListener} object listens on the pipe and behaves accordingly when events occur, including receipt of tweets and exceptions. More specifically, it stores the textual content of the message and metadata, such as \emph{geotags}. A \emph{Filter} object provides limited functionality for enforcing constraints on the types of tweets received; the only useful parameter that could be set was the topic\footnote{https://dev.twitter.com/docs/api/1.1/post/statuses/filter}. 
\subsection{The Data Corpus}
\label{data_corpus} 
The data collected for this project is divided into three corpora:
\begin{enumerate}
 	\item 
 	The \emph{Training and Evaluation Corpus}. The name of the corpus reflects its purpose: together with the associated polarity labels described in \autoref{data_annotation}, a part of these tweets are meant to serve as examples for the supervised learner, while the other part are used to evaluate the accuracy of the classifiers.
 	\item
 	The \emph{Experimental Corpus}. This corpus contains tweets which served as a basis for observation in the process of designing the systems for sentiment analysis. More specifically, these messages were analysed by the programmer in order to observe useful characteristics and features that could be exploited.
 	\item
 	The \emph{Mapping Corpus}. This corpus gathers geographically tagged tweets which are labelled by the supervised system and plotted on a world map to illustrate the distribution of opinion. 
\end{enumerate} 
The table below provides supplementary information about the data corpora. Also, \autoref{data_annotation} describes in more detail how the \emph{Training and Evaluation Corpus} was gathered and annotated, and concludes with a review on the structure of the data corpus. 
\begin{figure}[H]
\centering
	\begin{tabular}{|l|l|l|l|}
	\hline
	\textbf{Data Corpus} & \textbf{Collection Mechanism} & \textbf{Size} & \textbf{Type of Label}\\ \hline
	Training and Evaluation & Twitter Search API & 5,500 & Noisy and manual \\ \hline
	Experimental & Twitter Search API & 2000 & Noisy \\ \hline
	Mapping & Twitter Streaming API & 100,000 & By classifier \\ \hline
	\end{tabular}
\caption{Details about the Data Corpus.}
\end{figure}

\subsection{Database Schema}
\label{database}