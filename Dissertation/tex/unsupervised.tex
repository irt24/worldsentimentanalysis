\newpage

\section{Unsupervised Sentiment Analysis}
This section describes the implementation of the unsupervised system for sentiment analysis. The two techniques mentioned in \autoref{unsupervised_preparation} were adopted, namely the \emph{lexicon-based} method and a basic form of \emph{linguistic analysis}. 

\subsection{The lexicon-based technique}
\label{lexicons}
The implementation of this technique makes use of multiple opinion lexicons and one emoticon dictionary. The following description assigns labels that will be used later on to refer to each of the dictionaries (e.g. \emph{dictionary 1}), identifies their source, describes their contents and motivates why they have been chosen.
\begin{itemize}
	\item 
	\textbf{NRC Word-Emotion Association Lexicon} (\emph{dictionary 1}) - 14,182 entries
	\begin{itemize}
		\item 
		\textbf{Dictionary structure}: This lexicon takes into consideration eight emotions (anger, fear, anticipation, trust, surprise, sadness, joy and disgust) and two sentiments (positive and negative). Words are associated with a binary label (0 or 1) for each criteria. More details can be found in \cite{nrc1}. 
		\item
		\textbf{Scoring scheme}: Sentiment annotations suffice for the purpose of this project. The score of a word is computed by subtraction: \emph{positive score - negative score}. The score of a Tweet is obtained by adding up the scores of its components.
		\item
		\textbf{Motivation}: The reason for choosing this dictionary is that it provides a simple scoring mechanism and explicit labels for the two opposing polarities.
	\end{itemize}
	\item
	\textbf{General Inquirer} (\emph{dictionary 2}) - 8,632 entries
	\begin{itemize}
		\item 
		\textbf{Dictionary structure}: This dictionary defines two sets of markers (syntactic and semantic), which are subdivided into many subcategories\footnotemark[1]\footnotetext[1]{http://www.wjh.harvard.edu/~inquirer/kellystone.htm}. Entries are represented by words, associated with a list of terms that describe their syntactic and semantic properties. The relevant labels are `positive' and `negative'; the polarity of a word is simply marked by the presence or absence of the two strings. 
		\item 
		\textbf{Scoring scheme}: In order to bring this to a similar format to the above dictionary, the presence of the label `positive' scores 1, the presence of the label `negative' scores -1, and the absence of any of them scores 0. The sum of these numerical values represents the final polarity weight of a word. The score of the Tweet is computed as above.
		\item  
		\textbf{Motivation}: The resulting dictionary has the same format the one above, and this similarity will be exploited when evaluating the classifier. Also, its size is approximately half of \emph{dictionary 1} - characteristic that might reveal interesting insights into how the number of entries influences the accuracy of the classifier.
	\end{itemize}
	\item
	\textbf{SentiWordNet} (\emph{dictionary 3}) - 89,153 entries
	\begin{itemize}
		\item 
		\textbf{Dictionary structure}: SentiWordNet\footnotemark[1]\footnotetext[1]{http://sentiwordnet.isti.cnr.it/} is one of the most widely used lexicons in research on sentiment analysis (see \cite{sentiwordnet} and \cite{indiansentiwordnet}). It was derived from the WordNet\footnotemark[2]\footnotetext[2]{http://wordnet.princeton.edu/} database, and, in contrast with \emph{dictionary 1} and \emph{dictionary 2}, it has been generated automatically by linguistic and statistic classifiers. SentiWordNet exhaustively considers all meanings of words, but this feature cannot be exploited here (word sense disambiguation would be required). Therefore, only the first meaning of a word is considered, assuming that it is the most frequent one.
		\item  
		\textbf{Scoring scheme}: The lexicon associates three scores with each word, reflecting their polarity (positive, objective or negative), and adding up to 1. The final score of a Tweet is obtained by computing three different sums for each label mentioned before and choosing the maximum one. Note that this scoring scheme is not compatible with the two above.
		\item
		\textbf{Motivation}: This dictionary has been chosen because it provides a wider range of sentiment orientation and therefore is expected to perform differently than the first two. 
	\end{itemize}
	  
	\item
	\textbf{Affective Norms of English Words - ANEW} (\emph{dictionary 4}) - 1030 entries
	\begin{itemize}
		\item 
		\textbf{Dictionary structure}: The newest version of this dictionary is available for researches only, but an older one is made public in the associated research paper \cite{anew}. As \cite{anewuse} mentions, this lexicon rates words on three dimensions: psychological valence (good versus bad), arousal (active versus passive) and dominance (strong versus weak). Each word has been annotated by human coders with a numerical value from 1 to 9, with half point increments. 
		\item 
		\textbf{Scoring scheme}: For the purpose of this project, psychological valence is used as a measure of polarity. Experimentally, the most suitable thresholds for polarity proved to be 1-4 for positive, 4-7 for neutral and 7-9 for negative valence.
		\item 
		\textbf{Motivation}: This dictionary was chosen because, as \emph{dictionary 3}, it provides a more complex scoring scheme than just binary values. However, in contrast with the latter, it is limited to 1030 entries, which means that it only considers those words that are strongly polarised.
	\end{itemize}
	\item
	\textbf{Manual Dictionary} (\emph{dictionary 0}) \\
	The analysis on the \emph{experimental corpus} showed that a significant number of terms were not covered by any of the above dictionaries. Therefore, terms that do not occur in any of the above lexicons but appear three or more times in the previously mentioned corpus were considered for manual annotation. The resulting dictionary contains words associated with four values. The first value is either -1, 0 or 1 and aims to be compatible with \emph{dictionary 1} and \emph{dictionary 2}. The next three values have the same format as the one used by \emph{dictionary 3}. The occurrence of such terms can be justified as follows:
	\begin{itemize}
		\item 
		The lexicons used so far do not incorporate the Twitter slang. For instance, the word \emph{belieber} combines the terms \emph{believer} and \emph{(Justin) Bieber}, expressing positive sentiments toward the artist. In this case, the slang was added to the manual dictionary with a score of 1 for the first value and the next three numbers were taken from the entry \emph{believe} in \emph{dictionary 3}.
		\item
		The \emph{NLP library} does not produce the correct lemma for some words such as \emph{happier} or \emph{happiest}. To correct this shortcoming, such terms were added to the manual dictionary with scores taken from their lemmas (in this case \emph{happy}). Other examples include \emph{bigger}, \emph{dancing}, \emph{flooding}, etc.
		\item
		Common (possibly deliberate) misspellings, e.g. \emph{goodmorning}. Even if this seems to be an isolated case, more than 15 such occurrences were counted in the \emph{experimental corpus}. Each misspelling was treated differently depending on its nature. In this case, the score of the term was computed by averaging the scores for \emph{good} and \emph{morning}.
		\item
		Interjections such as \emph{hahaha} were treated by finding a corresponding dictionary term that is semantically close and associating the same score as the latter. For this particular case, the word \emph{laughter} was used.
	\end{itemize}
	All lexicons were merged this dictionary. From here on, the labels \emph{dictionary 1, dictionary 2} and \emph{dictionary 3} will refer to the result of composing original dictionaries with \emph{dictionary 0}.
	\item
	\textbf{Combined Dictionaries} (\emph{dictionary 12, dictionary 21}) \\
	For evaluation purposes, \emph{dictionary 1} and \emph{dictionary 2} were merge in two different ways, giving them alternative priorities. This means that entries that appear in both dictionaries draw their score from \emph{dictionary 1} to build \emph{dictionary 12} and the other way round for the second combination.
\end{itemize}

\subsection{Linguistic Analysis}
The lexicon-based technique is enhanced by using a linguistic analysis which considers negation. The following example from the \emph{experimental corpus} motivates why negation words should influence the decision of the classifier:
\begin{center}
\emph{And you don't care. You don't care or share my fears at all.}
\end{center}
Assume that, using one of the dictionaries that associate words with binary scores, we obtain the next scoreboard:
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
\hline
\textbf{Word} & care & care & share & fear \\ \hline
\textbf{Score} & 1 & 1 & 1 & -1 \\ \hline
\end{tabular} 
\end{center}
In this case, neglecting negation misleads the system towards a positive label, because of the positive polarities of the words \emph{care} and \emph{share}. This problem has been tackled in two ways, in order to determine the most suitable method:
\begin{enumerate}
	\item 
	Invert the score of the next word. In the given example, the scores of \emph{care} would be inverted, with a resulting value of -2 (negative). 
	\begin{center}
	\begin{tabular}{|l|l|l|l|l|}
	\hline
	\textbf{Word} & care & care & share & fear \\ \hline
	\textbf{Score} & -1 & -1 & 1 & -1 \\ \hline
	\end{tabular}
	\end{center}
	\item
	Invert the score of the whole sentence. In the Tweet cited above, the final score would still be -2 (negative).
	\begin{center}
	\begin{tabular}{|l|l|l|l|l|}
	\hline
	\textbf{Word} & care & care & share & fear \\ \hline
	\textbf{Score} & -1 & -1 & -1 & 1 \\ \hline
	\end{tabular}
	\end{center}
\end{enumerate} 