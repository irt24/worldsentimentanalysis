\chapter{Preparation}
This chapter presents different approaches to sentiment analysis and data collection. More specifically, it starts by describing the alternative methods of unsupervised and supervised learning, and continues by presenting the advantages and disadvantages of using microblogging messages and news articles as a data source. It then concludes on the trade-off made.

\section{The Unsupervised Approach}
\label{unsupervised_preparation}
Most sentiment analysis approaches can be split into \emph{unsupervised} and \emph{supervised} techniques. The first category makes no use of labelled data, which is its most significant advantage. It is best represented by the \emph{lexicon-based techniques}, which involve dictionaries of words precoded with polarity scores, named \emph{opinion lexicons}. As motivated in \cite{sentiwordnet}, they are often used in sentiment analysis under the assumption that a word is the minimal unit of opinion information. The overall score of a text is a function of the individual scores of its components.
\vsmallspace
Other unsupervised methods include \emph{linguistic analysis}, which detect sentiment polarity by looking at the grammatical structure of the text, potentially in parallel with using a lexicon. Indicators of polarity include negations, forms of adjectives (out of which the superlative is the most expressive) or sentiment-bearing idioms \cite{link4}. 
\vsmallspace
Due to its simple and naive approach, this method will only be used as a baseline and will not provide labels for the data plotted on the world map. 
\subsection{The Aho-Corasick Algorithm}
The unsupervised approach requires matching all the words that occur in a tweet against a pre-defined dictionary. Two different solutions to this issue were implemented, in order to decide which one performs the best. While the first alternative is quicker, the second one brings additional complexity, which results in more a more accurate classification:
\begin{enumerate}
	\item
	Treat the tweet as a block of text and identify the words in the dictionary that are its substrings. This approach makes use of the Aho-Corasick algorithm, described in the current section.
	\item 
	Treat the tweet as a set of lemmatised tokens (divide the text into its basic components and, in case of words, find their non-inflected form), then lookup each token in the dictionary. More details on this approach can be found in \autoref{implementation}.
\end{enumerate}
\subsubsection{Overview of the Algorithm}
\label{aho_corasick}
The Aho-Corasick algorithm is a classic solution to the task of \emph{exact set matching problem}, defined as follows:
\begin{problem}
	Locate occurrences of any pattern $P_i$ of a set $P = \{P_1,...P_k\}$ in a target $T[1...m]$.
\end{problem}
This technique is conveniently tailored for the specific situation when the set of patterns is fixed and known at compile time, and the target is short. The remainder of this subsection presents an overview of the algorithm, including its key steps and datastructures.
\vsmallspace
The algorithm is split into two stages. First, it constructs a finite state machine (FSM), based on a \emph{keyword tree} (or \emph{trie}) that stores all the patterns. Second, it uses the previously built datastructure to find all the occurrences of any pattern in the target. The search process is equivalent to transitions between states in the FSM.
\subsubsection{Building the Automaton}
The key of the algorithm is the fact that patterns are stored in a way which enables efficient pattern matching against the target. As mentioned above, the datastructure used for this purpose is an automaton based on a keyword tree, defined using the following: 
\begin{definition}
The \uline{label of an edge} is one of the characters occurring in any pattern of the pattern set.
\end{definition}
\begin{definition}
The \uline{label of a vertex} is the concatenation of the edge labels on the path from the root to the vertex.
\end{definition}
\begin{definition}
A \uline{keyword tree} is a rooted tree $K$ with labelled edges and vertices, such that:
\begin{spacing}{-0.75}
	\begin{enumerate}
	\item 
	any two edges out of a node have different labels;
	\item
	for each pattern $P_i$ in the pattern set, there is a vertex $v$ with the label $P_i$;
	\item
	the label of any leaf $v$ equals some pattern $P_i$.
\end{enumerate}
\end{spacing}
\end{definition}
The construction of the keyword tree for a pattern set $P$ begins by creating a root labelled by the empty symbol. Then, for every pattern $P_i$ in the set, the path labelled by the characters of $P_i$ is followed, starting from the root. Let the event of finding a character of $P_i$ along the path be called \emph{consumption}. The end of the process of going down a path is due to one of the following situations:
\begin{itemize}
	\item 
	The path ended before all the characters in $P_i$ were consumed. In this case, the path is extended by adding new edges, labelled by the remaining characters in $P_i$.
	\item
	All the characters in the pattern $P_i$ have been consumed. In this case, the identifier $i$ of $P_i$ is stored at the last node visited on the path.
\end{itemize}
After building the keyword tree, the next step in constructing the FSM is to add structure and functionality such that it behaves like an automaton. The elements of the FSM are defined below:
\begin{definition}
The \uline{alphabet} contains all the characters that occur in any pattern in the pattern set. Call this alphabet $\Sigma$.
\end{definition}
\begin{definition}
A \uline{state} is a node in the keyword tree. The set of states is denoted by $Q$.
\end{definition}
\begin{definition}
The \uline{initial state} is the root.
\end{definition}
\begin{definition}
The \uline{set of accepting states} is the set of all states associate with a pattern index. That is, states that are labelled by a pattern.
\end{definition}
\begin{definition}
The \uline{transitions} are determined by two functions: the goto function, and the failure function.
\end{definition}
The two functions which perform transitions are described below:
\begin{enumerate}
	\item 
	The \emph{goto function} $g(q,c)$ (where $q \in Q$ and $c \in \Sigma$) makes the transition to a state $q'$ such that the edge $(q, q')$ is labelled by the character $c$. If this function successfully returns a state in the tree, it means that a match has occurred. Otherwise, it returns the \emph{empty state}.
	\item
	The \emph{failure function} $f(q)$ is applied in case of mismatch, meaning that there is no edge outgoing from $q$ and labelled by $c$. The next state given by this function, $q'$, represents \emph{the longest proper suffix} $w$ of the label of $q$ such that $w$ is a prefix of some pattern $P_i$. An edge labelled by the empty symbol is added between $q$ and $q'$. Note that this function always returns a valid state, as the root (labelled by the empty character) is a proper suffix of any vertex label.
\end{enumerate}
Additionally, an \emph{output function out(q)} returns the set of patterns matched when entering state $q$, that is, all the patterns recognised by the algorithm before reaching this state (not necessarily on the path from root to $q$, as the \emph{failure} function performs transitions from one path to another).
\vsmallspace
At this stage, the datastructure can be compiled and stored for later use. Recompilation is required only when the set of pattern changes. The pattern matching algorithm described in the next subsection simply makes use of this already built FSM.
\subsubsection{Pattern Matching}
The second stage of the algorithm is the actual lookup of the patterns $P_i$ in the target. This is done by feeding $T$ as an input to the FSM. The target is scanned character by character from left to right. Every scanned character determines one or more transitions, as explained below. It must be emphasized that one single character can determine a whole chain of transitions, due to the existence of edges labelled by the empty symbol. 
\vsmallspace
The system starts in the initial state (the root of the keyword tree), and advances one state with each transition. The decision of the next transition is made by following a protocol: at first, the \emph{goto} function is called. If it returns the empty state, the \emph{fail} function is repeatedly applied until \emph{goto} returns a valid state. In other words, edges labelled by the empty symbol are followed until the character that is being considered in the target encounters an edge labelled by it. After such a successful transition occurs, the function $output$ called on the current state outputs all the patterns recognised so far.
\vsmallspace
The algorithm terminates when all the characters in the target have been scanned. At this stage, all the recognised patterns have been output and so the \emph{exact pattern matching} problem has been solved.


\section{The Supervised Approach}
\label{supervised_preparation} 
The supervised approach to sentiment analysis relies on standard machine learning: the system infers a model to solve the problem by looking at examples. The data corpus is therefore split into a training set $S_{train}$ and an evaluation set $S_{eval}$. The first consists of data annotated for polarity by human coders or by other means, while the second contains unseen text which is to be annotated by the machine. The features are usually represented by \emph{unigrams} (individual words), \emph{bigrams} (word pairs) or \emph{trigrams} (word triples) \cite{link4}. Statistics show that, at least for English, groups of more than tree words do not contribute to increasing accuracy. Depending on the textual domain, other features such as letter repetitions can be used. 
\vsmallspace
The supervised approach outscores the unsupervised one, as shown in \autoref{evaluation}. Therefore, it has been chosen to perform sentiment analysis on the opinionated messages plotted onto the world map.

\subsection{Support Vector Machines}
The supervised system for sentiment analysis employs the linear Support Vector Machine (SVM) algorithm. This algorithm was chosen because it was specifically designed for classification tasks and because it can be trained quickly. The next subsection describes SVM in more detail.

\subsubsection{Overview} 
SVM is an algorithm for supervised learning, capable of solving classification and regression problems. It employs a technique called \emph{the kernel trick} to represent data as points in a multi-dimensional space. Given that each point belongs to a class, the aim of the model is to find a hyperplane that separates the classes.
\vsmallspace
SVM comes in two flavours, namely linear and non-linear. The latter does not constrain the decision boundary to be linear, so it can capture more complex relations between the data inputs. However, it is more computationally expensive and does not bring any real benefits for the specific task of document classification. Therefore, the linear version of the algorithm has been chosen. 

\subsubsection{Theory}
This section illustrates the mathematical theory that underlies this algorithm, as compiled from \cite{svm}.
Let us make the following assumptions:
\begin{itemize}
	\item 
	There are $M$ training points. That is, the training set contains $M$ entries;
	\item 
	Each training point $x_i$ has dimensionality $N$ (i.e. the number of features associated with each entry in the training set is $N$);
	\item 
	Each training point $x_i$ is in one of the two classes $c_i \in \{-1,1\}$;
	\item 
	Data is linearly separable, meaning that the decision boundary is linear (given by a hyperplane).
\end{itemize}
The separation hyperplane can be expressed in terms of a set of points \textbf{x}, a normal to the hyperplane \textbf{w}, and a constant $b$ such that $\frac{b}{\vert \textbf{w} \vert}$ is the perpendicular distance from the hyperplane to the origin, using the following formula:
\begin{equation}
\textbf{w} \cdot \textbf{x} + b = 0
\end{equation}
Because the aim of SVM is to find the decision boundary which maximises the distance from to the nearest data point of any class, \textbf{w} and $b$ must be chosen such that the training data can be described by:
\begin{equation}
\textbf{x}_i \cdot \textbf{w} + b \ge +1 \qquad \text{for } c_i = +1
\end{equation}
\begin{equation}
\textbf{x}_i \cdot \textbf{w} + b \le -1 \qquad \text{for } c_i = -1
\end{equation}
The two equations can be combined and rewritten as:
\begin{equation}
c_i(\textbf{x}_i \cdot \textbf{w} + b) - 1 \ge 0 \qquad \text{for } i \in \{-1, 1\}
\end{equation}
Considering only those points that lie close to the separating hyperplane (the \emph{Support Vectors}), we obtain two parallel hyperplanes (call them $H1$ and $H2$) given by:
\begin{equation}
\textbf{w} \cdot \textbf{x} - b = \pm 1
\end{equation}
Let these hyperplanes be situated at distances $d1$ and $d2$ from the decision boundary, respectively. Orienting the separating hyperplane such that is as far away as possible form both $H1$ and $H2$ is equivalent to maximising the \emph{margin} value $d = d_1 = d_2$. 

\section{Data Source: \emph{Twitter} vs. \emph{Google News}}
\label{data_source}
This section describes the two approaches considered for constructing the data corpus for sentiment analysis and motivates the final choice of using \emph{Twitter} as a data source. 
\subsubsection{The Microblogging Paradigm} 
\label{microblogging}
The main criteria for collected data are subjectivity and uniform geographical distribution. The paradigm of \emph{microblogging} offers support in meeting these requirements. Microblogging is a relatively recent form of communication on the web, in which users broadcast information in short text messages. It represents a tremendous source of data for sentiment analysis \cite{link1}, because of the following main reasons: 
\begin{itemize}
	\item 
	\emph{Accessibility}: A computer with Internet connection suffices to ensure the physical means to convey information from any part of the world.
	\item
	\emph{Frequency of update}: The brevity of the posts reduces the amount of time and thought put into a message, which results in a significant number of posts, at frequent time intervals.
	\item
	\emph{Unlimited freedom of speech}: Users are not constrained in any manner in expressing their own views; there is no need for the approval of a publisher or for language discretion, as it is the case with news articles for instance.
	\item
	\emph{Subjectivity}: Independent of the user intention (daily chatter, conversations, sharing information/URLs or reporting news [1]), tweets generally show emotional involvement of their author, which can be translated into sentiment polarity.
\end{itemize}
\subsubsection{Twitter}
Data has been collected using Twitter\footnote{https://twitter.com/}, an online social networking and microblogging service. This platform provides users with personal feeds, which can be updated by their owners and followed by others. Hence, there is a real-time exchange of information among users, who can post and read short text messages (140 characters long), called tweets. The choice of this tool is motivated by its powerful, yet easy to use API.\footnote{https://dev.twitter.com/}
\subsubsection{Google News}
An alternative textual domain has been considered, namely news articles. The reason is that it provides the significant advantage of longer document length, with less noise. However, collecting a sufficient amount of news articles on a specific topic and obtaining enough geographical coverage proved to be a difficult task. The remainder of this section describes the attempts of collecting news articles for sentiment analysis.
\vsmallspace
For this purpose, the Google News Search API\footnote{https://developers.google.com/news-search/} was considered, as Google is notoriously known to possess impressive amounts of such textual data. Unfortunately, besides being officially deprecated, the functionality this API offers is unsatisfactory. The reasons are, as follows: 
\begin{itemize}
	\item 
	The only query parameters that can be set are keywords, so there is no way of selecting news from a particular geographical location. This fact has two consequences: first, more elaborate methods of natural language processing and/or information retrieval would be needed to extract or infer the geographical location of the author. Differentiating between the origin of the author and other potential mentions of places in the text would be another challenge. Second, even in the presence of such a mechanism that is capable of detecting where the author comes from, there is no guarantee that enough geographical coverage can be obtained.
	\item 
	Instead of providing the content of an article, the API retrieves only links to websites containing the corresponding articles. Parsing the HTML code with the intention to extract the meaningful data out of the metalanguage file did not give the best results. The parser, while performing well for some of the websites, failed to recognise the full text for others. Figure \ref{article} illustrates the two situations. The red rectangles represent the piece of text recognised as being part of the article. In the second case, the \emph{Recommended} section, which is most likely added by Google News, fragments the text and the rest of the article is mistakenly considered to be part of the addition.
\end{itemize}
\begin{figure}
\centering
\begin{subfigure}[l]{.49\textwidth}
  \fbox{\includegraphics[width=\textwidth]{img/data_collection/gnews_good.png}}
  \caption{Correct identification}
\end{subfigure}
\begin{subfigure}[r]{.49\textwidth}
  \fbox{\includegraphics[width=\textwidth]{img/data_collection/gnews_bad.png}}
  \caption{Incorrect identification}
\end{subfigure}
\caption{Identification of article body}
\label{article}
\end{figure}
Due to the above-mentioned impediments, the decision of using Twitter as a source for data was made. More details about data collection can be found in \autoref{data_collection_preparation}.

\section{Approaches to Data Collection}
\label{data_collection_preparation} 
\emph{Twitter} provides two means of collecting data. These approaches differ in their underlying implementation, and the queries that they can answer to are different. Due to the fact that they both show advantages and disadvantages, having almost a complementary behaviour, and because the opinion of \emph{Twitter} developers is quite scattered, two separate data collection engines have been implemented. The next two sections describe the technologies used by the APIs and the facilities they provide to the programmer who intends to retrieve data.

\subsection{Overview of the \emph{Twitter Search API}}
\label{search_api}
The Twitter Search API is based on the REST (Representational State Transfer) architectural model. With one query, the Twitter server retrieves up to 1500 relevant messages posted in the last week and puts them into JSON format. 
\subsubsection{Advantages}
The most important advantage of this API is that it enables fine-grained queries. For the purpose of this project, the most useful parameters are the key words meant to match a specific topic, the language - set to English -, and geographical location. The latter is specified by three numerical values: latitude, longitude, and radius, and should return tweets posted within the corresponding area.
\vsmallspace
Note, however, that this approach models each place of interest as a circle, so it is only an approximation of the real map of the world. A better alternative would have been to use bounding boxes, but they are not supported by the query format of the \emph{Twitter} API in discussion. 
\vsmallspace
In order to obtain an even geographical distribution of data, queries had to be made for each location. Separate requests were made for each topic/location pair. 
\subsubsection{Disadvantages}
This approach suffers from a significant flaw: some of the tweets seem to originate from more than one place. In other words, two queries with different location parameters may return the same set of posts. This might be a bug in the process of geographical location inference conducted by Twitter, or, alternatively, it might be a failure of the circle model associated with each place of interest. 
\vsmallspace
Practice shows that, when two tweets incorrectly have a double origin, the whole sets that they belong to suffer from the same problem. This makes the first error diagnosis more plausible, because the second would result in only partially overlapped sets, corresponding to the overlapping areas of the circles. 
\vsmallspace
An analysis of the misplaced posts shows that the countries and American states which are mistaken by \emph{Twitter} are grouped in geographical clusters. This information saves the collection engine from providing completely irrelevant information about spacial position: the tweets can be considered to originate from a certain group, rather than a specific state. Figure \ref{cluster} shows such a cluster, gathering multiple central American states.
\begin{figure}[H]
\centering
\fbox{\includegraphics[width=\textwidth]{img/data_collection/cluster.png}}
\caption{American states grouped in a cluster by the \emph{Twitter Search API}}
\label{cluster}
\end{figure}
Unfortunately, different topics determine slight changes in cluster membership. This final issue could be solved by using a clustering algorithm to find the most likely distribution of places. However, as it can be seen in Figure \ref{cluster}, the clusters cover quite a wide geographical area, which does not give enough granularity for a useful sentiment analysis. 
\vsmallspace
Other disadvantages of the API in discussion are listed below:
\begin{itemize}
	\item 
	The messages that can be obtained through one query are limited in number (one can request up to 15 `pages', each containing a maximum of 100 posts). Practice shows that requests are hardly ever fulfilled entirely, as the amount of messages received from the server is less than the one asked for.
	\item
	Messages face a time constraint (the age of the tweets can be no more than 6-9 days), so there so it is not possible to perform an analysis of evolution of opinion in time.
	\item
	Rate limiting puts even more restrictions on the collection engine, especially because the exact value is not known: `The Search Rate Limit isn't made public to discourage unnecessary search usage and abuse'\footnote{https://dev.twitter.com/docs/rate-limiting}.
\end{itemize}
\subsubsection{Conclusion}
In conclusion, this API is not useful for either obtaining a uniform, fine-grained distribution of data, nor gathering significant amounts of messages. Therefore, it was not engaged in collecting the data to be mapped on the world map. However, it has been noted that it provides a convenient way of collecting small sets of data that satisfy a well-defined set of parameters. Consequently, it was used for gathering the \emph{Training and Testing Corpus} and the \emph{Experimental Corpus} (described in \autoref{data_corpus}). These are relatively small labelled corpora which assisted in the process of designing and implementing the classifiers. 

\subsection{Overview of the \emph{Twitter Streaming API}}
\label{streaming_api}
The \emph{Twitter Streaming API} works on a different principle than the \emph{Twitter Search API}. A single HTTP request establishes a permanent connection with the Twitter servers, through which the client continuously receives tweets. This connection is maintained by an entity different from the HTTP handler application, namely a script running as a long-term background process. The same piece of software is responsible for consuming the messages received on the real time stream, in response to the user request.
\subsubsection{Advantages} 
Although this method is considerably more complex, it benefits from avoiding the overhead of polling a REST endpoint. Additionally, it is not rate limited and provides access to a larger amount of data, namely 1\% of all public posts. This percentage is misleadingly small, as the number of tweets posted in one day reaches the order of hundreds of millions. 
\subsubsection{Disadvantages}
The drawback of this approach is that queries are coarse-grained: there is no room for options, the only parameters that can be set are the keywords. Consequently, the two tasks of language and geographical filtering must be performed on the client side. The first issue is solved by checking every message against a dictionary and declaring it as being written in English after matching three words. The second problem is addressed by only including in the corpus the \emph{geotagged} tweets. That is, considering only those messages that have been tagged with geographical identification metadata by their authors, so that no location inference algorithm – potentially prone to errors – is involved. Unofficial sources approximate the amount of such posts to represent 0.5\% of all tweets in the stream.
\subsubsection{Conclusion}
In conclusion, this approach is more appropriate for collecting data with a uniform geographical distribution. The impediment of the reduced number of tweets that are geographically tagged can be overcome by keeping the connection open for a sufficient amount of time.