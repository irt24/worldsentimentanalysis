\chapter{Textual Contents Of The Website} 
\label{App:AppendixA}
This appendix contains the textual contexts of the website that hosts the graphical interface for manual annotation. It provides users with explanations about the aims of the project and guidelines on how to annotate.
\section{Introduction}
\subsection*{What is the purpose of this website?}
This website is part of my Tripos Part II project, that is, the final year dissertation for the Computer Science course that I am reading at the University of Cambridge.
\vsmallspace
The main focus of this project is to build a system for automatically identifying the sentiment held towards various issues in different parts of the world, by applying natural language processing (NLP) techniques and using artificial intelligence (AI) to analyse what people are posting on Twitter.
\vsmallspace
In order to evaluate the accuracy of the automated analysis, a standard methodology for data collection and evaluation in NLP requires for a representative random sample of tweets to be collected and annotated for their true sentiment by human judges. This website provides the graphical interface for the process of annotation.
\subsection*{What is the process of annotation?}
The process of annotation consists of associating each tweet with one of the following labels: `positive', `neutral', `negative' or `undecidable'. The label must illustrate the orientation of the opinion expressed in the message by the originator of the tweet. Participants should objectively attempt to decide the nature of the message, without expressing any personal opinion on the selected topics, even if in disagreement with the author's point of view. The decisions should solely be based on the language contained in the tweet.
\vsmallspace
More instructions about the labels: \uline{Guidelines}
\vsmallspace
Proceed to annotations: \uline{Annotations}
\vsmallspace
Please click the button `Stop annotating' after you decide to stop annotating.
\subsection*{Disclaimer}
Please note the following issues:
\begin{itemize}
	\item 
	The microblogging messages are collected from publicly visible tweets and may contain mentions of other people by their Twitter user names, but they have no intentional relation to the annotators.
	\item 
	The opinions expressed in the tweets reflect by no means the views of the project leader, project supervisor, University of Cambridge, or Computer Laboratory.
	\item 
	Because the tweets are intended to reflect diversity of opinion, special care has been taken to avoid offensive topics. The data was manually screened to check for uses of offensive language. However, should you find any such cases, please report them by typing the word `OFFENSIVE' in the comment box and they will be immediately removed from the data corpus.
	\item 
	There is no budget for compensating the annotators, so their work is voluntary.
\end{itemize}
\section{Guidelines}
\subsection*{General Guidelines}
Given an opinionated piece of text, wherein it is assumed that the overall opinion in it is about one single issue or item, the task of the annotator is to classify the author's view as falling under one of several sentiment polarities. That is, decide the polarity or semantic orientation of tweets with respect to a given topic. The overall semantic orientation must be deduced from language constructs, namely from the individual words that make up the tweet (Semantic orientation represents an evaluative characterisation of a word's deviation from the norm for its semantic group or lexical field - e.g. \emph{beautiful} is positively oriented, as opposed to \emph{ugly}).
\vsmallspace
Observation: in determining the sentiment polarity of opinionated texts where the authors do explicitly express their sentiment through statements like `this laptop is great', (arguably) objective information such as `long battery life' can be used to help determine the general sentiment.
\subsection*{Positive Label}
A tweet is positive if there are specific language constructs that indicate the presence of desirable features or qualities of an entity (person, object, event, situation etc.). 
\vsmallspace
Semantic indicators of a positive message are:
\begin{itemize}
	\item
	acceptance (\emph{He is one of ours now.})
	\item
	agreement (\emph{Justin Bieber really does have a good voice muah})
	\item
	approval (\emph{I'm voting 4 Justin :D})
	\item
	courage (\emph{Hurricane Sandy don't want none. This is Merica, we ain't scared.})
	\item
	idolatry (\emph{My life is explained in two simple words. Justin Bieber})
	\item
	improvement (\emph{Apple keeps designing better and better products.})
	\item
	interest (\emph{Omg!!! Justin Bieber has a new girl friend???})
	\item
	optimism (\emph{East coast stay safe during hurricane sandy!!})
	\item
	pleasing progress (\emph{Since the new president's election, there seems to be a significant economic growth.})
	\item
	pleasure (\emph{Nothing better than carving pumpkins to taylor swift, one direction, and justin bieber with my sister})
	\item
	respect (\emph{REAL MAN respect Justin Bieber})
\end{itemize}
\subsection*{Neutral Label}
A tweet is neutral if there are no specific language constructs that indicate its polarity. The general attitude of the author is unbiased, impartial, or even disinterested. Facts are simply stated, most likely meant to inform the readers about the existance of an entity or the occurrence of an event. There seem to be no opposing sides, or if there are, the author is not helping or supporting either of them.
\vsmallspace
Example: \emph{Sandy is speeding up, moving northwest at 25-30 mph. It could make landfall south NJ around 5PM. Hurricane winds approaching NJ, south NY}
\subsection*{Negative Label}
A tweet is negative if there are specific language constructs that indicate the presence of undesirable features or the lack of certain qualities of an entity (person, object, event, situation etc.). The general view on the entity discussed upon is that it is harmful or unwelcome. 
\vsmallspace
Semantic indicators of a negative message are:
\begin{itemize}
	\item
	confusion (\emph{i dont get it, some people have `Justin Bieber Fan' or `One Direction Fan' and then they'll rip your face off if you call them fans? wtf})
	\item
	disagreement (\emph{I still maintain that US Elections are a bit silly, on the basis that it would take more than 4yrs to change the biggest country in the world})
	\item
	fear (\emph{omg hurricane sandy is actually so scary would hate to prepare for something like that})
	\item
	pessimism (\emph{There's no chance that NY can face Hurricane Sandy.})
	\item
	lack (\emph{Romney has no respect for me as a homosexual})
	\item
	limitation (\emph{How can two party \#democracy be an example to the \#world. I wish to see more parties in \#US elections. American people's choice is limited.})
	\item
	misfortune (\emph{\@trueblue51 It is very unfortunate that due to our current political situation we can't vote in the US elections. I'd vote for Romney.})
	\item
	refusal (\emph{Obama is pushing the world into grave, though Romney is not much better. That's the tragedy of US elections - the choice between two evils. Under these
	conditions, I refuse to vote.})
	\item
	unwillingness (\emph{So, if Justin Bieber and Rebecca Black were drowning, and you could only save one, would you go to lunch or read the paper?})
\end{itemize}
\subsection*{Undecidable Label}
This label should only be used in the following situations:
\begin{itemize}
	\item 
	the annotator faces serious difficulties in understanding the tweet, because it was poorly written - the author did not manage to get a message across;
	\item
	the annotator does not understand the language, either because (s)he is not familiar with the slang or because (s)he does not know the meaning of certain language constructs (especially for non-native English speakers);
	\item
	the tweet is malformed for different reasons, e.g. the poster posted a random set of letters (However, this should not be the case, as the search engine fetches relevant messages and, furthermore, the data was manually screened);
\end{itemize}
This label should not be used when the annotator finds it hard to decide the polarity of a well-formed message. In such cases, please choose one of the most suitable labels and leave a comment explaining your difficulties.
