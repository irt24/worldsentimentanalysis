\newtheorem{problem}{Problem}
\newtheorem{definition}{Definition}
\newpage
\cleardoublepage     % just to make sure before the page numbering is changed
\setcounter{page}{1}
\pagenumbering{arabic}
%\pagestyle{headings}

\chapter{Introduction} 
This section describes the driving force for the project, motivates particular choices and enumerates practical applications. Also, it situates the project within the field of Computer Science and briefly describes previous contributions to this specific area.

\section{Aims and Motivation}
This project aims to analyse the sentiment captured in messages collected from Twitter, and visually represent the geographical distribution of opinions on a world map. There are four main aspects that need to be discussed in order to motivate this choice: 
\begin{enumerate}
	\item 
	\textbf{The practical purposes of sentiment analysis}. Sentiment expressed in textual units provides a valuable indicator of how people perceive different forms of reality (events, persons, products, etc.). Detecting and analysing opinion in an automated way provides a useful tool for gathering evidence on the general attitude towards a specific topic. A precise definition of sentiment analysis can be found in \autoref{sentiment_polarity}. 
	\item 
	\textbf{The benefits of gathering data from a microblogging platform}. In order to obtain an accurate view on the way people feel about an entity of interest, the platform used for expressing opinion should have certain properties: easily accessible, dynamic (frequently updated), and without imposing any constraints to the writer. The microblogging paradigm supports all these features. A more elaborate discussion can be found in \autoref{microblogging}.
	\item 
	\textbf{The global geographical scale}. The choice of collecting data from the whole world gives a very broad view on what people think. This approach is beneficial in areas such as politics, where developing election strategies and alliances can require more than possessing knowledge about a limited geographical area. Also, in the field of marketing, online opinion has become a `virtual currency'\footnote{http://en.wikipedia.org/wiki/Sentiment\_analysis}, as multinational companies can speculate opportunities and track the global perception on their products. Social research in general can also benefit from this feature.
	\item 
	\textbf{The visual illustration of the results}. Plotting the output of the analysis on a visual map and making use of suggestive colours for different feelings summarises the statistics in a more efficient and expressive way than other representations such as tables or charts. The main reason is that the correspondence between location and sentiment orientation is concise and clear.
\end{enumerate}

\section{Area of Computer Science}
\label{area}
This section states the problems solved by this project and proves its focus on a specific field in computer science, namely natural language processing.
\subsection{The Classification Problem}
\label{classification}
The project addresses the vast problem of \emph{classification}, which encompasses numerous meanings in different disciplines and even within a single field. In a broad sense, it refers to the task of assigning the appropriate label to each object in a set. In the context of natural language processing, the objects to be classified are textual units, and the classes are (possibly infinite) sets of objects \cite{class}, associated with a label which reveals a meaningful characteristic of the object that is being classified.

\subsection{Sentiment Polarity}
\label{sentiment_polarity}
The classification problem addressed by the project is that of \emph{sentiment analysis}, an application of natural language processing which aims to extract subjective information from a source material. The problem was stated more formally by \cite{main} and can be summarised as follows:

\begin{problem}
Given an opinionated text and making the assumption that there is only one entity that generates opinion, classify the sentiment expressed by the author, either on a discrete or continuous scale. 
\end{problem} 

Most of the work done so far defines the scale to be either a fixed set of labels, or specifies a numeric interval in which the sentiment can oscillate. The first choice leads to the more specific problem called \emph{sentiment polarity}, which is approached in this project:

\begin{problem}
Given an opinionated text and making the same assumption as before, classify the document as either positive, neutral or negative, according to the overall opinion expressed by the author. 
\end{problem}

The word \emph{polarity} will be used interchangeably with the terms \emph{valence} and \emph{semantic orientation} to refer to one of the labels mentioned in Problem 2. A more elaborate description of the three choices of polarity is given in \autoref{website}.
\vsmallspace
Other approaches use sets of mood states such as tension, depression, anger, vigour, fatigue, and confusion \cite{link3}, or valence, arousal and dominance \cite{anew}.

\section{Previous Related Work}
