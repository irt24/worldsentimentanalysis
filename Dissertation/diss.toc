\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Aims and Motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Area of Computer Science}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}The Classification Problem}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Sentiment Polarity}{2}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Previous Related Work}{3}{section.1.3}
\contentsline {chapter}{\numberline {2}Preparation}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}The Unsupervised Approach}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}The Aho-Corasick Algorithm}{4}{subsection.2.1.1}
\contentsline {subsubsection}{Overview of the Algorithm}{5}{section*.8}
\contentsline {subsubsection}{Building the Automaton}{5}{section*.9}
\contentsline {subsubsection}{Pattern Matching}{7}{section*.10}
\contentsline {section}{\numberline {2.2}The Supervised Approach}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Support Vector Machines}{7}{subsection.2.2.1}
\contentsline {subsubsection}{Overview}{8}{section*.11}
\contentsline {subsubsection}{Theory}{8}{section*.12}
\contentsline {section}{\numberline {2.3}Data Source: \emph {Twitter} vs. \emph {Google News}}{9}{section.2.3}
\contentsline {subsubsection}{The Microblogging Paradigm}{9}{section*.13}
\contentsline {subsubsection}{Twitter}{9}{section*.14}
\contentsline {subsubsection}{Google News}{10}{section*.15}
\contentsline {section}{\numberline {2.4}Approaches to Data Collection}{10}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Overview of the \emph {Twitter Search API}}{11}{subsection.2.4.1}
\contentsline {subsubsection}{Advantages}{11}{section*.17}
\contentsline {subsubsection}{Disadvantages}{12}{section*.18}
\contentsline {subsubsection}{Conclusion}{13}{section*.20}
\contentsline {subsection}{\numberline {2.4.2}Overview of the \emph {Twitter Streaming API}}{13}{subsection.2.4.2}
\contentsline {subsubsection}{Advantages}{13}{section*.21}
\contentsline {subsubsection}{Disadvantages}{14}{section*.22}
\contentsline {subsubsection}{Conclusion}{14}{section*.23}
\contentsline {chapter}{\numberline {3}Implementation}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Data Collection}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}The \emph {Twitter 4J Library}}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Data Collection Using the \emph {Twitter Search API}}{16}{subsection.3.1.2}
\contentsline {subsubsection}{General framework}{16}{section*.24}
\contentsline {subsubsection}{Collecting Data for Mapping}{16}{section*.26}
\contentsline {subsubsection}{Collecting small sets of data}{17}{section*.29}
\contentsline {subsection}{\numberline {3.1.3}Data Collection Using the \emph {Twitter Streaming API}}{17}{subsection.3.1.3}
\contentsline {subsubsection}{General Framework}{18}{section*.30}
\contentsline {subsubsection}{Implementation Details}{18}{section*.32}
\contentsline {subsection}{\numberline {3.1.4}The Data Corpus}{19}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Database Schema}{20}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Data Annotation}{21}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Noisy Labels}{21}{subsection.3.2.1}
\contentsline {subsubsection}{Emoticon queries}{21}{section*.34}
\contentsline {subsubsection}{User timelines}{21}{section*.35}
\contentsline {subsection}{\numberline {3.2.2}Manual Labels}{22}{subsection.3.2.2}
\contentsline {subsubsection}{Noise Elimination}{22}{section*.36}
\contentsline {subsubsection}{Graphical Interface for Manual Annotation}{22}{section*.37}
\contentsline {subsubsection}{The Manually-Annotated Subcorpus}{24}{section*.39}
\contentsline {subsubsection}{Review of the Data Corpus}{24}{section*.40}
\contentsline {section}{\numberline {3.3}Data Preprocessing}{25}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Preprocessing Operations}{25}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Code Overview}{27}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Unsupervised Sentiment Analysis}{28}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}The lexicon-based technique}{28}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Linguistic Analysis}{30}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Supervised Sentiment Analysis}{32}{section.3.5}
\contentsline {subsubsection}{Features}{32}{section*.43}
\contentsline {section}{\numberline {3.6}Mapping Sentiment}{33}{section.3.6}
\contentsline {chapter}{\numberline {4}Textual Contents Of The Website}{34}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{34}{section.4.1}
\contentsline {section}{\numberline {4.2}Guidelines}{35}{section.4.2}
