use strict;
use warnings;
use DBI;
use DBD::mysql;
use Term::ReadKey;
use Text::CSV;

#####################
# Auxiliary methods
#####################
sub do{
	my $dbh = $_[0];
	my $stmt = $_[1];
	return $dbh->do($stmt) or die ($dbh->errstr);
}

#####################
# Create Database
#####################
print "Database name: ";
chomp(my $db_name = <STDIN>);

print "Database host: ";
chomp(my $db_host = <STDIN>);

print "Database user: ";
chomp(my $db_user = <STDIN>);

print "Password: ";
ReadMode('noecho');
chomp(my $db_pass = <STDIN>);
ReadMode(0);
print "\n";

my $dbh = 	DBI->connect("dbi:mysql:", $db_user, $db_pass)
			or die("One of the parameters is wrong");
my $stmt = "CREATE DATABASE $db_name";
my $result = &do($dbh, $stmt);
if ($result) {print "Database created.\n";}
$stmt = "USE $db_name";
$result = &do($dbh, $stmt);

#####################
# Create Tables
#####################
print "Creating the table...\n";

$stmt = <<"CREATE_TABLE_TWEETS";
CREATE TABLE TWEETS (
	ID VARCHAR(30) NOT NULL PRIMARY KEY,
	TEXT VARCHAR(200) CHARACTER SET utf8mb4,
	TOPIC VARCHAR(200) CHARACTER SET utf8mb4,
	GEOLOCATION VARCHAR(100)
)
CREATE_TABLE_TWEETS
$result = &do($dbh, $stmt);
if ($result) {print "Table TWEETS was successfully created.\n";}
print "Done!\n";