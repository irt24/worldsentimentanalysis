use strict;
use warnings;
use DBI;
use DBD::mysql;
use Term::ReadKey;

#####################
# Connect to database
#####################
print "Step 1: Connect to a database:\n";
print "Database name: ";
#chomp(my $db_name = <STDIN>);
my $db_name = "sentiment_analysis";

print "Database host: ";
#chomp(my $db_host = <STDIN>);
my $db_host = "localhost";

print "Database user: ";
#chomp(my $db_user = <STDIN>);
my $db_user = "root";

print "Password: ";
#ReadMode('noecho');
#chomp(my $db_pass = <STDIN>);
#ReadMode(0);
#print "\n";
my $db_pass = "raluplus";

my $dbh = 	DBI->connect("dbi:mysql:$db_name:$db_host", $db_user, $db_pass)
			or die("Could not connect to database");
if ($dbh) {print "Connected to database!\n\n"};

######################
# Set query parameters
######################
sub read01 {
	my $message = $_[0];
	my $keepGoing = 1;
	my $input;
	while ($keepGoing) {
		print $message;
		chomp($input = <STDIN>);
		if (($input eq "1") or ($input eq "0")) {
			$keepGoing = 0;
		} else {
			print "Format not recognised.\n";
		}
	}
	return $input;
}

sub readDate {
	my $message = $_[0];
	my $keepGoing = 1;
	my $input;
	while ($keepGoing) {
		print $message;
		chomp($input = <STDIN>);
		if ($input =~ /\d{4}-\d{2}-\d{2}/) {
			$keepGoing = 0;
		} else {
			print "Format not recognised.\n";
		}
	}
}

sub toBoolStr {
	my $val = $_[0];
	if ($val) {return "true";}
	else {return "false";}
}

#print "Step 2: Set query parameters:\n";
#
#print "Query string (use underscore instead of spaces): ";
#chomp(my $queryString = <STDIN>);
#
#my $countries = &read01("Fetch tweets from countries?(0/1): ");
#my $states = &read01("Fetch tweets American states?(0/1): ");
#
#print "Number of tweets / location: ";
#chomp(my $ntweets = <STDIN>);
#
#my $startDate = &read01("Is there a start date for the tweets?(0/1): ");
#if ($startDate) {$startDate = &readDate("Type in the starting date in the format 'YYYY-MM-DD': ");}
#
#my $endDate = &read01("Is there an end date for the tweets?(0/1): ");
#if ($endDate) { $startDate = &readDate("Type in the ending date in the format 'YYYY-MM-DD': ");}
#	
#print "You have made the following choices: ";
#print "\n".$queryString;
#print "\n".$countries;
#print "\n".$states;
#print "\n".$ntweets;
#print "\n".$startDate;
#print "\n".$endDate;
#print "\nDone!";

my $updateStr = `java DT $queryString $countries $states $ntweets $startDate $endDate`;

