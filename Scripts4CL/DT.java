public class DT {
    
    private static String queryString, startDate, endDate;
    private static boolean countries, states;
    private static int ntweets;

    public static void main(String[] args) throws Exception {

    	// set query parameters
        if (args.length < 6) {
            throw new Exception("Missing arguments.");
        }
        queryString = args[0].replaceAll("_"," ");
        countries = args[1].equals("1");
        states = args[2].equals("1");
        ntweets = Integer.parseInt(args[3]);
        startDate = args[4];
        endDate = args[5];
        //System.out.println(queryString+" " + countries + " " + states + " " + ntweets + " " + startDate + " " + endDate);
    }
}
