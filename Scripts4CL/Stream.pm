use strict;
use warnings;
use DBI;
use DBD::mysql;
use Term::ReadKey;

#####################
# Connect to database
#####################
print "*************************************************************************************************";
print "Step 1: Connect to a database:\n";
print "Database name: ";
chomp(my $db_name = <STDIN>);
#my $db_name = "sentiment_analysis";

print "Database host: ";
chomp(my $db_host = <STDIN>);
#my $db_host = "localhost";

print "Database user: ";
chomp(my $db_user = <STDIN>);
#my $db_user = "root";

print "Password: ";
ReadMode('noecho');
chomp(my $db_pass = <STDIN>);
ReadMode(0);
print "\n";
#my $db_pass = "asdf";

#####################
# Set query string
#####################
print "*************************************************************************************************";
print "Step 2: Set query string:\n";
print "Please note the following guidelines: \n";
print "1. The query string consists of a series of track phrases; \n";
print "2. Track phrases should be separated by commas, with no spaces (i.e.: phrase1,phrase2,phrase3); \n";
print "3. Phrases should consist of one or more words, separated by underscores; \n";
print "4. Example: christmas,new_year,holiday \n";
print "Query string: ";
chomp(my $query_string = <STDIN>);

#################################
# Compile and call the Java code
#################################
system("javac -classpath core.jar:stream.jar:mysql.jar:. StreamingCL.java");
system("java -classpath core.jar:stream.jar:mysql.jar:. StreamingCL $db_name $db_host $db_user $db_pass $query_string");

